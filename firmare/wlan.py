# module to setup and manage wifi connection

import network
import uasyncio as asyncio
from errors import ErrorSignal
from config import WifiCredentials


# timers for connection checks - seconds
_WLAN_ERR_DISCONNECTED_LIMIT = const(30)
_WLAN_ERR_RECONNECT_PERIOD = const(15)
_WLAN_ERR_CHECK_PERIOD = const(1)


class Wlan(WifiCredentials):
    def __init__(self):
        self.past_status = None  # it has to start at something
        network.hostname(self.hostname)

        self.wlan = network.WLAN(network.STA_IF)
        self.wlan.active(True)
        # self.wlan.config(pm = 0xa11140) # power saving off ?

        if self.use_static_ip:
            static_connect = (self.ip_address, self.subnet, self.gateway, self.dns)
            self.wlan.ifconfig(static_connect)

        if not self.wlan.isconnected():
            self.wlan.connect(self.ssid, self.password)

        # periodic connection check
        self.wlan_err_counter = 0
        self.past_status = self.wlan.status()


    # run periodic check if wlan is connected and restart if invalid for a long time
    async def periodic_checking(self, online_indicator=None):
        try:
            while True:
                await asyncio.sleep(_WLAN_ERR_CHECK_PERIOD)

                status = self.wlan.status()
                # correct connected state
                if (status in (network.STAT_IDLE, network.STAT_GOT_IP)):
                    online_indicator.set()
                    if status != self.past_status:
                        await ErrorSignal.clear()  # this may also cancel error signal that is not wifi related

                # critical status - connection could not be established
                elif (status in (network.STAT_WRONG_PASSWORD, network.STAT_NO_AP_FOUND, network.STAT_CONNECT_FAIL)):
                    self.wlan_err_counter += 1
                    online_indicator.clear()
                    if status != self.past_status:
                        await ErrorSignal.set(ErrorSignal.ERR_CODE['wlan_critical_error'])

                # looking for connection
                else:
                    self.wlan_err_counter += 1
                    online_indicator.clear()
                    if status != self.past_status:
                        await ErrorSignal.set(ErrorSignal.ERR_CODE['wlan_connecting'])

                # wifi is disconnected for too long - try to reconnect
                self.past_status = status
                if self.wlan_err_counter >= _WLAN_ERR_DISCONNECTED_LIMIT:
                    self.wlan_err_counter = 0
                    await self.reconnect()

        except asyncio.CancelledError:
            # mostly ignore cancel error because it should only break infinite loop
            online_indicator.clear()


    # reconnection procedure
    async def reconnect(self):
        self.wlan.disconnect()
        await asyncio.sleep(_WLAN_ERR_RECONNECT_PERIOD)
        self.wlan.connect(self.ssid, self.password)


    # shorthand to ommit "wlan" in the call
    def is_connected(self):
        return self.wlan.isconnected()
