# core1 procedure that runs http request server and starts several asyncio loops
import micropython
import sys
import gc
import uasyncio as asyncio
import _thread
import re

import config
import temperature
from onewire import OneWireError
import wlan
import rtc
import display
from errors import ErrorSignal


micropython.alloc_emergency_exception_buf(100)

SERVER_MAX_CONNECTIONS = const(3)
SERVER_LOOP_PERIOD = const(2)

# --- request responses ---
http_ok = 'HTTP/1.1 200 OK\r\nContent-type: text/json;charset=UTF-8\r\n\r\n'
http_nok = 'HTTP/1.1 400 Bad Request\r\nContent-type: text/plain;charset=UTF-8\r\n\r\n'
# --- ---

# process http request from the client
async def handle_client_request(reader, writer):
    try:
        gc.collect()  # prepare some room for the http messages
        request = await reader.read(512)

        if request == b'':  # probably some prematurely closed connection from client side
            pass

        # GET request
        # return temperatures to host system
        elif request.startswith(b'GET /Temperature'):
            writer.write(http_ok)
            await writer.drain()
            await proc_temperature.sock_send_temperatures(writer)
            writer.write('\r\n')
            await writer.drain()

        # return only last temperatures
        elif request.startswith(b'GET /t'):
            writer.write(http_ok)
            await writer.drain()
            hour, minute = await clock.time()
            t_in, t_out = await proc_temperature.last_tin_tout()
            writer.write('{{\r\n\t"clock": "{0:02d}:{1:02d}",\r\n\t"T_unit": "°C",\r\n\t"T_in": {2},\r\n\t"T_out": {3}\r\n}}\r\n'.format(hour, minute, t_in, t_out))
            await writer.drain()

        # empty display - useful to call before shutdown
        elif request.startswith(b'GET /Clear'):
            writer.write(http_ok)
            await writer.drain()
            await screen.update(None)

        # PUT request
        # upload hour and minute offset for localtime compared to ntp utc time
        elif request.startswith(b'PUT /Timezone'):
            try:
                tz_hour = int(
                    re.search('tz_hour[^-^0-9]*(-?\d*)', request).group(1))
                tz_minute = int(
                    re.search('tz_minute[^-^0-9]*(-?\d*)', request).group(1))
                writer.write(http_ok)
                await writer.drain()
                await clock.update(tz_hour, tz_minute)

            except (AttributeError, ValueError):
                writer.write(http_nok)
                writer.write('Incorrect message content.\r\n')
                await writer.drain()

        # INVALID request
        else:  # do not uderstand the request
            writer.write(http_nok)
            await writer.drain()
            writer.write('Not implemented.\r\n')
            await writer.drain()

        # can be called multiple times without problem (precaution that some case might miss it)
        await writer.drain()

    # error with the connection (e.g. closed from the client side)
    except OSError:
        await ErrorSignal.set(ErrorSignal.ERR_CODE['request_os_error'], repeat=1)
    finally:
        writer.close()
        await writer.wait_closed()


# async request server
async def server_runner():
    ip_server = config.WifiCredentials.ip_address[0] if config.WifiCredentials.use_static_ip == True else '0.0.0.0'
    server = await asyncio.start_server(handle_client_request,
                                        ip_server,
                                        config.WifiCredentials.port,
                                        backlog=SERVER_MAX_CONNECTIONS)
    async with server:
        while True:  # prevent this function exit
            await asyncio.sleep(SERVER_LOOP_PERIOD)


# main runner - starts several infinite loops in async mode
async def main():
    # mark some instances as global since they are used in several async procedures
    # it is easier than constantly passing the reference to them
    # there is only one instance of each and they carry large buffers - do not create a copy
    global proc_temperature
    global clock
    global screen

    try:
        clock = rtc.Clock()  # local time handling

        # temperature handling object - shared between multiple async runners
        try:
            proc_temperature = temperature.TemperatureW1()
        except MemoryError:
            await ErrorSignal.set(ErrorSignal.ERR_CODE['temperature_memory_error'])
            sys.exit()  # break main loop since something serious broke
        except (temperature.InvalidSensor, OneWireError):
            await ErrorSignal.set(ErrorSignal.ERR_CODE['missing_temperature_sensor'])
            sys.exit()  # device cannot work without temperature sensors

        try:
            screen = display.Display(clock, proc_temperature)
        except MemoryError:
            await ErrorSignal.set(ErrorSignal.ERR_CODE['screen_memory_error'])
            sys.exit()  # break main loop since something serious broke

        wifi = wlan.Wlan()  # connect to wifi

        online_flag = asyncio.Event()  # share status of wifi status with ntp

        await asyncio.gather(server_runner(),
                             wifi.periodic_checking(online_flag),
                             rtc.ntp_sync(online_flag),
                             proc_temperature.temperature_reading(),
                             screen.periodic_update()
                             )

    except MemoryError:
        await ErrorSignal.set(ErrorSignal.ERR_CODE['main_loop_memory'])
        raise
    except Exception:
        await ErrorSignal.set(ErrorSignal.ERR_CODE['main_loop'])
        raise


# main entry point for the script call
if __name__ == '__main__':
# start error signaling in second thread as soon as possible
    if config.ErrorHandling.enabled:
        second_thread = _thread.start_new_thread(ErrorSignal.err_runner, ())

    try:
        asyncio.run(main())
    finally:
        if config.ErrorHandling.enabled:
            if config.ErrorHandling.break_with_main:
                second_thread.exit()
        asyncio.new_event_loop()
