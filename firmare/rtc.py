# module to make RTC time handling easier
import time
import ntptime
import uasyncio as asyncio
from errors import ErrorSignal


# attempt time sync every 5 minutes
# some source claim maximum period is 6 minutes, another 70 minutes and another source several hours
_NTP_TIMER_PERIOD = const(300) # sync time every n seconds
_NTP_MINUTES_IN_HOUR = const(60)
_NTP_HOURS_IN_DAY = const(24)


# periodic time synchronisation calls
async def ntp_sync(online_indicator):
    while True:
        await online_indicator.wait()
        try:
            ntptime.settime()
        except OSError:
            await ErrorSignal.set(ErrorSignal.ERR_CODE['ntp_sync'], repeat=3)
        except:
            await ErrorSignal.set(ErrorSignal.ERR_CODE['ntp_sync'], repeat=1)
        await asyncio.sleep(_NTP_TIMER_PERIOD)


# unified time format
def timestamp(since=None):
    (year, month, day,
     hour, minute, second,
     weekday, yearday) = time.gmtime()

    if since is None: # return only timestamp
        return (year%100, month, day, hour, minute, second)
    else: # return timestamp and seconds 'since'
        now = time.time()
        diff = now - since
        return ((year%100, month, day, hour, minute, second), now, diff)


# clock with local time keeping
class Clock():
    # keep clock differences for localtime - rely on host system
    tz_hour = 0
    tz_minute = 0
    clock_lock = asyncio.Lock()


    # update internal values
    async def update(self, tz_hour, tz_minute):
        async with self.clock_lock:
            self.tz_hour = int(tz_hour)
            self.tz_minute = int(tz_minute)


    # get clock in localtime
    async def time(self):
        async with self.clock_lock:
            (year, month, day,
             hour, minute, second,
             weekday, yearday) = time.gmtime()

            # minute correction
            local_minute = minute + self.tz_minute
            minute_overflow = 0
            while local_minute >= _NTP_MINUTES_IN_HOUR:
                local_minute -= _NTP_MINUTES_IN_HOUR
                minute_overflow += 1
            while local_minute < 0:
                local_minute += _NTP_MINUTES_IN_HOUR
                minute_overflow -= 1

            # hour correction
            local_hour = hour + self.tz_hour + minute_overflow
            while local_hour >= _NTP_HOURS_IN_DAY:
                local_hour -= _NTP_HOURS_IN_DAY
            while local_hour < 0:
                local_hour += _NTP_HOURS_IN_DAY

            return (local_hour, local_minute)
