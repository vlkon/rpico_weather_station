# ePaper2in13V3.py nanogui driver for Pico-ePpaper-2.13
# Tested with RPi Pico
# EPD is subclassed from framebuf.FrameBuffer for use with Writer class and nanogui.
# Optimisations to reduce allocations and RAM use.
#
# Released under the MIT license see LICENSE
# Thanks to @Peter for a great micropython-nano-gui: https://github.com/peterhinch/micropython-nano-gui
#
# -----------------------------------------------------------------------------
# * | File        :   ePaper2in13V3.py
# * | Author      :   Waveshare team
# * | Function    :   Electronic paper driver
# * | This version:   V1.0
# * | Date        :   2022-10-10
# -----------------------------------------------------------------------------
#
#
# strongly modified for async mode
# used with e-ink display v2 from Waveshare
#

from machine import Pin, SPI
import framebuf
import uasyncio as asyncio
import time
import gc

_WIDTH_EXACT = const(122)  # 122
# 122 -> 122/8=15.25 -round-up-> 16 bytes==128 bits (16 bytes can hold 122 pixels)
_WIDTH = const(128)
_HEIGHT = const(250)

# SPI connection on pico
_SPI_ID = const(0)
_SPI_BAUDRATE = const(4_000_000)
_MOSI_PIN = const(19)
_MISO_PIN = const(16)
_SCK_PIN = const(18)

# signal pins on pico
_RST_PIN = const(21)
_DC_PIN = const(20)
_CS_PIN = const(17)
_BUSY_PIN = const(22)

# timing
_EPD_H_RESET_DELAYS_MS = const(200)
_EPD_READY_CHECK_PERIOD_MS = const(20)

# look-up table for full update - do not add intentation inside byte string
_lut_full_update = b'''\
\x80\x60\x40\x00\x00\x00\x00\
\x10\x60\x20\x00\x00\x00\x00\
\x80\x60\x40\x00\x00\x00\x00\
\x10\x60\x20\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x00\x00\
\x03\x03\x00\x00\x02\
\x09\x09\x00\x00\x02\
\x03\x03\x00\x00\x02\
\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00'''

# look-up table for partial update - do not add intentation inside byte string
_lut_partial_update = b'''\
\x00\x00\x00\x00\x00\x00\x00\
\x80\x00\x00\x00\x00\x00\x00\
\x40\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x00\x00\
\x0A\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00'''


class EPD_2in13_v2(framebuf.FrameBuffer):

    WHITE = const(0xFF)
    BLACK = const(0x00)

    # enumerator placeholders
    UPDATE_FULL = const(0)
    UPDATE_PART = const(1)


    def __init__(self):
        # init signal pins
        self._rst = Pin(_RST_PIN, Pin.OUT)
        self._rst(0)  # disable display as default state just in case
        self._busy = Pin(_BUSY_PIN, Pin.IN, Pin.PULL_UP)
        self._cs = Pin(_CS_PIN, Pin.OUT)
        self._dc = Pin(_DC_PIN, Pin.OUT)

        # init SPI
        self._spi = SPI(_SPI_ID, baudrate=_SPI_BAUDRATE,
                        sck=Pin(_SCK_PIN, Pin.OUT),
                        mosi=Pin(_MOSI_PIN, Pin.OUT),
                        miso=Pin(_MISO_PIN, Pin.IN, Pin.PULL_UP))

        # Public bound variables required by nanogui.
        self.width = _WIDTH
        self.width_exact = _WIDTH_EXACT
        self.height = _HEIGHT

        # Set immediately on start of task. Cleared when busy pin is logically false (physically 1).
        self._updating_lock = asyncio.Lock()
        self._updated = asyncio.Event()

        # frame buffer
        gc.collect()
        self._buffer = bytearray(self.height * self.width // 8)
        self.image = memoryview(self._buffer)
        super().__init__(self.image, self.width, self.height, framebuf.MONO_HLSB)


    # clear and turn off display on exit
    def __del__(self):
        self.clear()
        self.sleep()
        self._rst(0)


    # send command to the display followed by data if specified
    def _command(self, command, data=None):
        self._dc(0)
        self._cs(0)
        self._spi.write(command)
        self._cs(1)
        if data is not None:
            self._data(data)


    # send daty byte to the display
    def _data(self, data, buf1=bytearray(1)):
        self._dc(1)
        for b in data:
            self._cs(0)
            buf1[0] = b
            self._spi.write(buf1)
            self._cs(1)


    # hardware reset using reset pin
    async def hardware_reset(self):
        self._rst(1)
        await asyncio.sleep_ms(_EPD_H_RESET_DELAYS_MS)
        self._rst(0)
        # blocking since it is very short and very long reset may screw up the reset procedure
        time.sleep(5)
        self._rst(1)
        await asyncio.sleep_ms(_EPD_H_RESET_DELAYS_MS)


    # main epd init procedure (also important after wake from sleep mode)
    async def _epd_init(self):
        await self.hardware_reset()

        if(self.update_type == self.UPDATE_FULL):  # full update init
            await self.wait_until_ready()
            self._command(b'\x12')  # soft reset
            await self.wait_until_ready()

        # common init for full and partial update
        self._command(b'\x74', b'\x54')  # set analog block control
        self._command(b'\x7e', b'\x3b')  # set digital block control
        # driver output control - 0xf9+1 == 250 (y-size), gate G0->G295, no interlock, incrementing
        self._command(b'\x01', b'\xF9\x00\x00')
        # data entry mode - x-increment, y-increment, x-direction first
        self._command(b'\x11', b'\x03')
        # set Ram-X address start/end position
        # end 0x0f-->(15+1)*8=128 (can hold 122 pixels of x-axis)
        self._command(b'\x44', b'\x00\x0F')
        # set Ram-Y address start/end position
        # start 0 / end 0xf9-->(249+1)=250
        self._command(b'\x45', b'\x00\x00\xF9\x00')
        # position cursor
        self._command(b'\x4E', b'\x00')  # set RAM x address counter to 0
        # set RAM y address counter to 0
        self._command(b'\x4F', b'\x00\x00')

        if(self.update_type == self.UPDATE_FULL):  # full update init
            self._command(b'\x3C', b'\x03')  # BorderWavefrom - LUT3
            self._command(b'\x18', b'\x80')  # read built-in temperature sensor
            self._command(b'\x2C', b'\x54')  # VCOM Voltage: -2.1 VCOM
            # set gate drive voltage - 0x15: 19 VGH
            self._command(b'\x03', b'\x15')
            # set source drive voltage -  0x41: 15 VSH1, 0xa8: 5 VSH2, 0x32: -15 VSL
            self._command(b'\x04', b'\x41\xA8\x32')
            # dummy Line period - 0x30: default
            self._command(b'\x3A', b'\x30')
            # gate line width (time) - 0x0a: default 50Hz
            self._command(b'\x3B', b'\x0A')
            self._command(b'\x32', _lut_full_update)  # write LUT register
            await self.wait_until_ready()

        else:  # partial update init
            await self.wait_until_ready()
            self._command(b'\x2C', b'\x24')  # VCOM Voltage -0.9
            await self.wait_until_ready()
            self._command(b'\x32', _lut_partial_update)  # write LUT register
            # write register for display option - 0x40: ping-pong function for display mode 2
            self._command(b'\x37', b'\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00')
            # write register for display option
            self._command(b'\x22', b'\xC0')
            self._command(b'\x20')  # display option activate
            await self.wait_until_ready()
            self._command(b'\x3C', b'\x01')  # border waveform control - LUT1


    # check if the display is busy - testing hardware pin status
    async def wait_until_ready(self):
        await asyncio.sleep_ms(5)  # safety delay to allow busy pin to change state
        while self._busy() == 1:
            await asyncio.sleep_ms(_EPD_READY_CHECK_PERIOD_MS)


    # decide which mode of update should be done
    def _update_type(self, update_type):
        if update_type == self.UPDATE_PART:
            self.update_type = self.UPDATE_PART
        else:
            self.update_type = self.UPDATE_FULL


    # send buffer content to epd and do full update display
    # update_type==None clears the display
    async def display(self, update_type=None):
        # lock other actions on the display until init is done
        await self._updating_lock.acquire()
        try:
            if update_type is None: # clear display
                self.fill(self.WHITE)

            self._update_type(update_type)  # prepare for full or partial update
            await self._epd_init()  # init display for update

            # put image to display 1 buffer
            self._command(b'\x24', self.image)

            # put inversed image to display 2 buffer - important for partial update
            self._command(b'\x26')
            buf_byte = bytearray(1)
            for b in self.image:
                # inverse value for partial update
                buf_byte[0] = ~b if self.update_type == self.UPDATE_PART else b
                self._data(buf_byte)

            # call display procedure based on previous init call
            if(self.update_type == self.UPDATE_FULL):
                # display update control - Enable Clock Signal, Then Enable ANALOG C7, Then DISPLAY with DISPLAY Mode 1, Then Disable ANALOG, Then Disable OSC
                self._command(b'\x22', b'\xC7')
                self._command(b'\x20')  # master activation
                await self.wait_until_ready()
            else:
                # display update control - To DISPLAY with DISPLAY Mode 2
                self._command(b'\x22', b'\x0C')
                self._command(b'\x20')  # master activation
                await self.wait_until_ready()

        finally:
            await self._epd_sleep(update_type)  # always disable display to prevent damage
            self._updating_lock.release()


    # deep sleep function - after that epd does not work without another init call
    async def _epd_sleep(self, update_type=None):
        await self.wait_until_ready()
        # display update control - disable analog and clock
        self._command(b'\x22', b'\xc3')
        self._command(b'\x20')  # master activation
        self._command(b'\x10', b'\x01')  # deep sleep 1
        # in case of "display clear" call (used only when device is powered down)
        if update_type is None:
            # disabling power every time require full init every time - partial does not work correctly
            # only sleep procedure b'\x10' should be enough
            self._rst(0)  # According to schematic this turns off the power
        # this is not too important - it just guarantees that the display is not called too often
        await asyncio.sleep_ms(500)
