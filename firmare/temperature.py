from machine import Pin
import gc
import array
import onewire
import ds18x20
import json
import uasyncio as asyncio
import rtc
from config import TemperatureSensors
from errors import ErrorSignal


_BUFFER_LENGTH = const(2048)  # how many reading cycles can buffer hold - empirical value
# how many decimal places whould temperature reading have
_PRECISION = const(0.01)  # temperature reading decimal precision
_TIMESTAMP_LEN = const(6)  # year%100, month, day, hour, minute, second
_BUFSIZE_TIME = const(_BUFFER_LENGTH * _TIMESTAMP_LEN)
_PERIOD_MS_MIN = const(750)  # minimal ms period between trigger and read for DS18B20

# --- exceptions ---
class InvalidSensor(Exception):
    pass
# --- ---


class TemperatureW1(TemperatureSensors):

    temperature_dummy = -99.9  # nonsense value that makes easier to filter wrong readings

    def __init__(self):
        gc.collect()
        # allocate shared memory for temperature
        self.temperatures = array.array('i', (0 for _ in range(_BUFFER_LENGTH * self.SENSOR_LIST_LEN)))
        # allocate shared memory for timestamps
        self.timestamps = array.array('B', (0 for _ in range(_BUFSIZE_TIME)))
        self.buf_index = 0  # pointer to active buffer place
        # create access lock to prevent undefined read/write from multiple calls
        self.buf_lock = asyncio.Lock()
        # indicate that the buffer is full and do not accept any more data
        self.buf_full = asyncio.Event()

        # hardware pin that connects ds18x20 with the mcu
        self.data_line = Pin(self.ONE_WIRE_PIN)
        self.ds = ds18x20.DS18X20(onewire.OneWire(self.data_line))
        self.check_sensors()  # throw an critical error if some sensor is not connected

        # storage for last inside/outside temperature reading
        # start with nonsense value that is valid float
        self.temperature_in = self.temperature_dummy
        self.temperature_out = self.temperature_dummy


    # verify that all sensors are connected
    def check_sensors(self):
        sensors_present = self.ds.scan()  # names of connected devices ds18x20
        for (sensor, _) in self.sensors:
            if sensor not in sensors_present:
                # very bad error - should break the main program flow
                # since it is at the device init the user should see it right away
                # and act accordingly
                raise InvalidSensor(f'Sensor "{sensor}" is not connected.')



        # try:
        #     self.ds.convert_temp()  # trigger first sensor reading because all are present
        # except:



    # procedure to get temperature values from sensor to the buffers
    # in case there is an error during reading the reading is dropped
    # there is no point to recover partial reading since it would only make
    # a mess in the database
    async def temperature_reading(self):
        period = self.ONE_WIRE_PERIOD if self.ONE_WIRE_PERIOD >= _PERIOD_MS_MIN else _PERIOD_MS_MIN
        conversion_valid = False
        while True:
            try:
                self.ds.convert_temp()  # trigger sensors
                conversion_valid = True
            except:
                conversion_valid = False
                await ErrorSignal.set(ErrorSignal.ERR_CODE['temperature_sensor_reading'], repeat=2)

            finally:
                # delay before the sensor is ready to send data back to host
                # and also delay between unsuccesful convert loop
                # they will not start too soon and flood the device
                await asyncio.sleep_ms(period)  # wait period before next temp read

            if conversion_valid:
                try:
                    await self.fetch_temperatures()  # add new reading to the buffer
                except:  # sensor probably disconnected or crc error
                    await ErrorSignal.set(ErrorSignal.ERR_CODE['temperature_sensor_reading'], repeat=3)


    # get temperature readings from all sensors
    async def fetch_temperatures(self):
        async with self.buf_lock:  # prevent read/write collision on buffers
            if not self.buf_full.is_set():
                try:
                    # request timestap
                    tmstmp = array.array('B', rtc.timestamp())
                    self.timestamps[(_TIMESTAMP_LEN * self.buf_index):
                                    (_TIMESTAMP_LEN * (self.buf_index + 1))] = tmstmp

                    # request temperatures
                    for i, (sensor, name) in enumerate(self.sensors):
                        tmprtr = self.ds.read_temp(sensor)  # float value

                        # special designated sensors
                        if name == 'T_in':
                            self.temperature_in = tmprtr
                        if name == 'T_out':
                            self.temperature_out = tmprtr

                        # append value to buffer
                        # store as integer to make the buffer easy to manage
                        self.temperatures[(self.SENSOR_LIST_LEN *
                                           self.buf_index) + i] = int(tmprtr / _PRECISION)
                except:
                    # ignore any error that happened during temperature read
                    # just drop this loop of reading
                    await ErrorSignal.set(ErrorSignal.ERR_CODE['temperature_sensor_reading'], repeat=1)
                else:
                    # advance buffer pointer
                    self.buf_index += 1
                    if self.buf_index >= _BUFFER_LENGTH:
                        self.buf_full.set()


    # return temperature reading of designated sensors (mainly for display)
    async def last_tin_tout(self):
        async with self.buf_lock:
            return (self.temperature_in, self.temperature_out)


    # flush all readings to the caller
    async def get_latest(self):
        # check if the buffer is in use and prevent read
        async with self.buf_lock:
            # parse buffer
            if self.buf_index > 0:
                time_buf = tuple(self.timestamps[_TIMESTAMP_LEN * (self.buf_index - 1):
                                                 _TIMESTAMP_LEN * self.buf_index])
                temp_buf = tuple(self.temperatures[self.SENSOR_LIST_LEN * (self.buf_index - 1):
                                                   self.SENSOR_LIST_LEN * self.buf_index])
            else:
                # empty buffer
                time_buf = None
                temp_buf = None

            ret_index = self.buf_index - 1
            return ret_index, self.buf_full.is_set(), time_buf, temp_buf


    # send temperatures as json to socket
    # do not combine together many strings to prevent memory allocation problem
    # it is better to create more smaller writes
    async def sock_send_temperatures(self, writer):
        # prevent further write to the buffer before this function returns
        async with self.buf_lock:
            # descriptors
            writer.write('{"temperature_header":')
            writer.write(json.dumps([name for (_, name) in self.sensors]))
            writer.write(',"temperature_multiplier":{0}'.format(_PRECISION))
            writer.write(
                ',"timestamp_format":["year", "month", "day", "hour", "minute", "second"]')
            await writer.drain()  # checkpoint - wait for the header send

            # timestamps section of json file
            writer.write(',"timestamps":[')  # open timestamp section
            for i in range(self.buf_index):
                if i != 0:
                    writer.write(',')
                writer.write(json.dumps(tuple(self.timestamps[_TIMESTAMP_LEN * i:
                                                              _TIMESTAMP_LEN * (i + 1)])))
                await writer.drain()
            writer.write(']')  # close timestamp section

            # temperatures section of json file
            writer.write(',"temperatures":[')  # open temperatures section
            for i in range(self.buf_index):
                if i != 0:
                    writer.write(',')
                writer.write(json.dumps(tuple(self.temperatures[self.SENSOR_LIST_LEN * i:
                                                                self.SENSOR_LIST_LEN * (i + 1)])))
                await writer.drain()
            writer.write(']')  # close temperatures section

            # closing section of json file
            writer.write('}')
            await writer.drain()  # checkpoint - send closing of json file

            # reset buffer
            self.buf_index = 0
            self.buf_full.clear()
