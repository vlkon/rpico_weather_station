# i/o module for drawing on the e-ink display
import uasyncio as asyncio
import epd_2in13_v2
import rtc
from errors import ErrorSignal


# e-ink display is recommended to refresh every 180s or later
# partial update is every minute because the clock will be pointless otherwise
_DISPLAY_FULL_EVERY_N_MINUTES = const(3)


# wrapper class for font definitions
# fonts are read from binary files on flash memory
class Font():
    def __init__(self, family, width, height):
        self.width = width
        self.height = height
        self.base_dir = '/fonts/{0}/{1:03d}x{2:03d}'.format(family, width, height)


    def char_path(self, character):
        return '{0}/char_{1:03d}x{2:03d}_{3:03d}.raw'.format(self.base_dir,
                                                             self.width,
                                                             self.height,
                                                             ord(character))


class Display(epd_2in13_v2.EPD_2in13_v2):

    FONT_CLOCK = Font('iosevka_extended_bold', 56, 80)
    FONT_TEMPERATURE = Font('iosevka_extended_bold_italic', 16, 27)

    # clock position origin (top left corner of the rectangle)
    HOUR_POS = (8, 5)
    MINUTE_POS = (8, 95)

    # temperature position origin (top left corner of the rectangle)
    TEMP_IN_POS = (0, 183)
    TEMP_OUT_POS = (0, 214)


    def __init__(self, clock, temperature):
        self.clock = clock
        self.temperature = temperature
        super().__init__()


    # reset framebuffer before additional write
    def clear_canvas(self):
        self.fill(self.WHITE)


    def build_gui(self):
        # the function updates frames into the buffer but does not display
        for i in range(2):
            # outer border
            self.rect(i, i, self.width_exact - 2*i, self.height - 2*i, self.BLACK, False)
            # clock-temperature horizontal separator
            self.hline(0, 180-i, self.width_exact, self.BLACK)


    # print clock to framebuffer
    async def build_clock(self):
        font = self.FONT_CLOCK
        (hour, minute) = await self.clock.time()
        hx, hy = self.HOUR_POS
        mx, my = self.MINUTE_POS

        for i, c in enumerate('{0:02d}'.format(hour)):
            self.write_char(font, c, hx + i*font.width, hy)

        for i, c in enumerate('{0:02d}'.format(minute)):
            self.write_char(font, c, mx + i*font.width, my)


    # print temperature to framebuffer
    async def build_temperature(self):
        font = self.FONT_TEMPERATURE
        (inside, outside) = await self.temperature.last_tin_tout()
        ix, iy = self.TEMP_IN_POS
        ox, oy = self.TEMP_OUT_POS

        for i, c in enumerate('{0: 5.1f}°C'.format(inside)):
            self.write_char(font, c, ix + i*font.width, iy)

        for i, c in enumerate('{0: 5.1f}°C'.format(outside)):
            self.write_char(font, c, ox + i*font.width, oy)


    # write character from file to framebuffer
    def write_char(self, font, character, x_start, y_start):
        with open(font.char_path(character), 'rb') as f:
            for y in range(font.height):
                self.image[(y_start + y) * (self.width // 8) + (x_start // 8):
                           (y_start + y) * (self.width // 8) +
                           (x_start // 8) + (font.width // 8)
                           ] = f.read(font.width // 8)


    # rewrite the display
    async def update(self, mode=None):
        if mode in (self.UPDATE_FULL, self.UPDATE_PART):
            self.clear_canvas()
            await self.build_clock()
            await self.build_temperature()
            self.build_gui()

        await self.display(update_type=mode)  # (mode == None) clears display


    # display update procedure
    async def periodic_update(self):
        await self.update(self.UPDATE_FULL)
        screen_update_last = rtc.timestamp(0)  # default init value

        try:
            while True:
                screen_update_this = rtc.timestamp(screen_update_last[1])
                minutes_advanced = screen_update_this[0][4] != screen_update_last[0][4]

                # full update every x minutes
                if (minutes_advanced and
                    ((screen_update_this[0][4] % _DISPLAY_FULL_EVERY_N_MINUTES) == 0)):
                    await self.update(self.UPDATE_FULL)
                    screen_update_last = screen_update_this

                # partial update every minute
                elif minutes_advanced:
                    await self.update(self.UPDATE_PART)
                    screen_update_last = screen_update_this

                await asyncio.sleep(1)

        except MemoryError:
            await ErrorSignal.set(ErrorSignal.ERR_CODE['screen_memory_error'], repeat=3)

        finally:
            # clears display when no argument is specified
            await self.update(None)
