# error signalisation with build-in LED running on the second thread
# since there is only one LED then the signals may get overwritten

# from machine import Pin, Timer
from machine import Pin
import time
import uasyncio as asyncio
import _thread
from config import ErrorHandling


# 'global' setup
# speed for blinking pattern - delays in ms
ERR_DELAY_IDLE = const(50)
ERR_DELAY_BETWEEN_PATTERN = const(200)
ERR_DELAY_AFTER_PATTERN = const(1000)
# default values
ERR_DEFAULT_PATTERN = const(0)
ERR_DEFAULT_REPEAT = const(-1)


class ErrorSignal(ErrorHandling):

    # enumeration of error codes - number of blinks
    ERR_CODE = {
        'wlan_connecting': 1,
        'wlan_critical_error': 2,

        'request_os_error': 3,
        'ntp_sync': 4,
        'temperature_sensor_reading': 5,

        'main_loop': 7,
        'main_loop_memory': 8,
        'screen_memory_error': 9,
        'temperature_memory_error': 10,
        'missing_temperature_sensor': 11
    }

    async_lock = asyncio.Lock()  # prevent collision between async setters
    err_lock = _thread.allocate_lock()
    err_pattern = ERR_DEFAULT_PATTERN
    err_repeat = ERR_DEFAULT_REPEAT

    # init led for signaling
    err_led = Pin(ErrorHandling.err_pin, Pin.OUT)
    led_toggle_count = 0


    # indicate that the error happened
    @classmethod
    async def set(cls, pattern, repeat=ERR_DEFAULT_REPEAT):
        if cls.enabled:
            async with cls.async_lock:
                with cls.err_lock:
                    cls.err_pattern = pattern
                    cls.err_repeat = repeat


    # cancel error indication - externaly callable wrapper
    @classmethod
    async def clear(cls):
        if cls.enabled:
            async with cls.async_lock:
                with cls.err_lock:
                    cls._clear()


    # cancel error indication - must not be called from outside due to err_lock
    @classmethod
    def _clear(cls):
        cls.err_pattern = ERR_DEFAULT_PATTERN
        cls.err_repeat = ERR_DEFAULT_REPEAT
        cls.err_led.off()


    # master infinite function for the second thread - led blinking
    @classmethod
    def err_runner(cls):
        cls.err_led.off()  # just in case - default state is led.off

        while True:
            with cls.err_lock:
                pattern = cls.err_pattern

            if pattern == ERR_DEFAULT_PATTERN:  # no error - loop to wait for error signal
                time.sleep_ms(ERR_DELAY_IDLE)

            else:
                with cls.err_lock:
                    # countdown repeats to automaticaly disable blinking
                    if cls.err_repeat > 0:
                        cls.err_repeat -= 1
                    elif cls.err_repeat == 0:
                        cls._clear()

                    pattern = 2 * cls.err_pattern  # 2 edges ber blink (on/off)

                    # blink the whole pattern - expected that starting state is led.off
                    for _ in range(pattern):
                        cls.err_led.toggle()
                        time.sleep_ms(ERR_DELAY_BETWEEN_PATTERN)

                # outside of err_lock - allow new error signal to overwite the old one
                time.sleep_ms(ERR_DELAY_AFTER_PATTERN)
