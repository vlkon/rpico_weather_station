from PIL import Image, ImageFont, ImageDraw
from pathlib import Path

# use a truetype font (.ttf)
out_path = Path(__file__).parent / 'fonts'

font_families = {'iosevka_regular':                 '~/.fonts/iosevka/iosevka-mono-serif-regular.ttf',
                 'iosevka_extended':                '~/.fonts/iosevka/iosevka-mono-serif-extended.ttf',
                 'iosevka_extended_bold':           '~/.fonts/iosevka/iosevka-mono-serif-extendedbold.ttf',
                 'iosevka_extended_italic':         '~/.fonts/iosevka/iosevka-mono-serif-extendeditalic.ttf',
                 'iosevka_extended_bold_italic':    '~/.fonts/iosevka/iosevka-mono-serif-extendedbolditalic.ttf',
                 }

# x_side should be divisible by 8
font_sizes = [
    {'y_size': 16,  'x_size': 8},
    {'y_size': 30,  'x_size': 16},
    {'y_size': 27,  'x_size': 16},
    {'y_size': 40,  'x_size': 24},
    {'y_size': 80,  'x_size': 56},
]


font_height_scale_factor = 1.20
bw_threshold = 180

# some options
font_color = '#000000'  # HEX black
background_color = '#ffffff'  # Hex white
suffix_img = 'bmp' # use name without dot
suffix_raw = '.raw' # use dot-name


# Copy Desired Characters from Google Fonts Page and Paste into variable
# desired_characters = "ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzž1234567890‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*"
# desired_characters = ''.join(chr(i) for i in range(32, 127)) # printable ascii chars
desired_characters = '-. 0123456789°C'


# Loop through the characters needed and save to desired location
def generate_image(character, font_family, ttf, x_size, y_size):

    # Create Font using PIL
    font_height = int(y_size * font_height_scale_factor)

    font = ImageFont.truetype(str(Path(ttf).expanduser().absolute()), font_height)
    # Get text size of character
    (left, top, right, bottom) = font.getbbox(character)
    width = right - left
    height = bottom - top
    x_offset = (width-x_size) // 2

    # print(character, ' ',
    #       f'w_{width}',
    #       f'h_{height}',
    #       '\t',
    #       f'l_{left}',
    #       f'r_{right}',
    #       f'b_{bottom}',
    #       f't_{top}')

    # Create PNG Image with that size
    img = Image.new('RGB', (x_size, y_size), color=background_color)
    draw = ImageDraw.Draw(img)

    # Draw the character
    draw.text((-x_offset,
               -(font_height-y_size)),
              str(character), font=font, fill=font_color)

    try:
        # convert grayscale to black and white
        img = img.convert('L').point(lambda x: 0xff if x > bw_threshold else 0x00, mode='1')
        (out_path / f'_{suffix_img}' / f'{font_family}' / f'{x_size:03d}x{y_size:03d}').mkdir(parents=True, exist_ok=True)
        img.save(out_path / f'_{suffix_img}' / f'{font_family}' / f'{x_size:03d}x{y_size:03d}' / f'img_{x_size:03d}x{y_size:03d}_{ord(character):03d}.{suffix_img}')
        return img

    except:
        print(f"[-] Couldn't Save:\t{character}")


# get binary file representation from image - read image from disk (easier to modify by hands)
def convert_img_to_bytearray(character, font_family, x_size, y_size):
    path_img = out_path / f'_{suffix_img}' / f'{font_family}' / f'{x_size:03d}x{y_size:03d}' / f'img_{x_size:03d}x{y_size:03d}_{ord(character):03d}.{suffix_img}'
    img_buffer = bytearray(y_size * (x_size // 8))

    with Image.open(path_img) as image:
        img = image.load()
        for y in range(y_size):
            for x in range(x_size):
                # x pixels are grouped in bytes
                if (x % 8) == 0:
                    px = 0

                # 1:white, 0:black
                if img[x, y] != 0:
                    px |= (1 << (7 - (x % 8)))

                # store to bytearray when byte is complete
                if x % 8 == 7:
                    img_buffer[(x // 8) + (y*(x_size//8))] = px
    return img_buffer


def save_raw(byte_arr, font_family, x_size, y_size):
    # save bytearray to a file in raw format
    (out_path / f'{font_family}' / f'{x_size:03d}x{y_size:03d}').mkdir(parents=True, exist_ok=True)
    with open(out_path / f'{font_family}' / f'{x_size:03d}x{y_size:03d}' / f'char_{x_size:03d}x{y_size:03d}_{ord(character):03d}{suffix_raw}', 'wb') as f:
        f.write(byte_arr)


def save_hex(byte_arr, font_family, x_size, y_size):
    # bytearray printed as hex (byte) representation
    (out_path / '_hex' / f'{font_family}' / f'{x_size:03d}x{y_size:03d}').mkdir(parents=True, exist_ok=True)
    with open(out_path / '_hex' / f'{font_family}' / f'{x_size:03d}x{y_size:03d}' / f'hex_{x_size:03d}x{y_size:03d}_{ord(character):03d}.txt', 'w') as f:
        for y in range(y_size):
            for x in range(x_size//8):
                byte_val = byte_arr[x + y*(x_size//8)]
                f.write(f'{byte_val:02x}')

                if x == (x_size//8)-1:
                    f.write('\n')
                else:
                    f.write(' ')


def save_bin(byte_arr, font_family, x_size, y_size):
    # bytearray printed as bite representation
    (out_path / '_bin' / f'{font_family}' / f'{x_size:03d}x{y_size:03d}').mkdir(parents=True, exist_ok=True)
    with open(out_path / '_bin' / f'{font_family}' / f'{x_size:03d}x{y_size:03d}' / f'bin_{x_size:03d}x{y_size:03d}_{ord(character):03d}.txt', 'w') as f:
        for y in range(y_size):
            for x in range(x_size//8):
                byte_val = byte_arr[x + y*(x_size//8)]
                f.write(f'{byte_val:08b}')

                if x == (x_size//8)-1:
                    f.write('\n')
                else:
                    f.write(' ')


if __name__ == '__main__':

    for font_family, ttf in font_families.items():
        for font_size in font_sizes:
            x_size = font_size['x_size']
            y_size = font_size['y_size']

            print(f'--- generating font {x_size:03d}x{y_size:03d} ---')
            for character in desired_characters:
                img = generate_image(character, font_family, ttf, x_size, y_size)
                barr = convert_img_to_bytearray(character, font_family, x_size, y_size)

                save_raw(barr, font_family, x_size, y_size)
                save_hex(barr, font_family, x_size, y_size)
                save_bin(barr, font_family, x_size, y_size)
