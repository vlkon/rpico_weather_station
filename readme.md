# Raspberry Pico W - weather station

A webserver with a raspberry pico w at its core.

Main purpose of this device is:
- measure temperatures from several DS18B20 sensors
- provide measured values via http request calls to a client
- showing temperature and clock on e-ink display

---

Main procedure starts several infinite loops in asynchronous mode.
Errors are showed with an LED running on core 1 if enabled in config.


## Setup
- Upload micropython binaries .uf2 file to a raspberry pico w.
    The device can be connected to pc with bootsel button as external drive.
    It is included in "mycropython" directory in case further releases are not compatible.

- Create "firmware/config.py" from "firmware/confing.py.template" - set correct ip and wifi credentials.

- Uploat everything from "firmware" directory to raspberry pico.
    You can use Thonny IDE to upload the files.
    Upload "main.py" as the last file - it can make the device unresponsive.
    In case there is a screw-up the device can be cleared with "utils/flash_nuke.uf2" file.

- Custom fonts can be generated with a help from a "utils/font_generate.py" script.
    Which font will be used on the device is defined in "firmware/display.py".


## Known limitation
- **Device has to be on a secure network because there is not firewall protection or other precautions.**
- Onboard LED cannot be used because it interfere with wifi connection.
- Screen updating period is recommended to 180 s but updating clock every 3 minutes does not make
    much sense the interval is used only for full update and partial update is every minute
    It may cause faster display deterioration (unconfirmed).
    Partial refresh leaves some ghosting, especially on large characters.
    Ghosting may be imroved with differnt LUT and voltage selection but there is significant lack of documentation.
- There is not "machine.Timer" because for some unknown reason it broke the device after some time of running
- Temperature DS18B20 sensor has minimal reading time 750 ms and musn't be shorter
- Buffer for temperature reading is limited to (8*2500) values
    (8*2048) is stable for several hours with multiple simultaneous fast http requests
    (8*3072) will not start because the memory allocation fails
- Microcontroller does not have enough processing power to handle localtime so the time is keep in utc.
    Localtime offset has to be provided from a client side.
