#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import signal
from datetime import datetime
import requests
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
from pathlib import Path
import json


CFG_FILE = Path(__file__).parent / 'config.json'
INFINITE_LOOP = True
SLEEP_TIME_LOOP = 10 * 60


# %% sigterm handling
def exit_gracefully(*arg):
    global INFINITE_LOOP
    INFINITE_LOOP = False


# %% get temperatures from the server in a form of json
def request_temperatures(get_call):
    r = requests.get(get_call, timeout=60)
    return r.json()


# %% build dataframe from json data
def process_temperatures(req_json):
    if not req_json['temperatures']:
        # req_json['timestamps] doesn't have to be tested since it is guaranteed by the rpico
        return None  # signal that no data were received

    else:
        # concatenate header and data blocks
        headers = req_json['timestamp_format'] + req_json['temperature_header']
        data = np.concatenate((req_json['timestamps'],
                               req_json['temperatures']), axis=1)

        dff = pd.DataFrame(data, columns=headers)

        # convert raw to real values - due to hardware limitation rpico should not do it
        for t_col in dff.columns:
            if t_col.startswith('T'):
                dff[t_col] *= req_json['temperature_multiplier']

        # process time information
        dff['year'] += 2000  # add offset that is not present in the request get
        dff['timestamp_utc'] = pd.to_datetime(dff[['year', 'month', 'day',
                                                   'hour', 'minute', 'second']],
                                              utc=True)

        # return only part of the dataframe that matches the database
        df = dff[['timestamp_utc'] +
                 [x for x in dff.columns if x.startswith('T')]]

        # filter out invalid timestamp and temperature - should be very small percentage
        df = df[(df['timestamp_utc'] > '2022-01-01')]
        df = df[df[[x for x in df.columns if x.startswith(
            'T')]].gt(-90).all(axis=1)]

        return df


# %% database connection engine
def create_connection(database, user, password='', host='localhost',
                      port=3306):
    return create_engine(f'mysql+pymysql://'
                         f'{user}:{password}'
                         f'@{host}:{port}/{database}',
                         echo=False)


# %% send dataframe to database
def temperature_store(cfg):
    # data fetch
    req_json = request_temperatures(
        f'http://{cfg["server"]["host"]}:{cfg["server"]["port"]}/{cfg["server"]["get_temperature"]}')

    # data transformation
    df = process_temperatures(req_json)

    if df is not None:
        # database upload
        con = create_connection(database=cfg['database']['database'],
                                user=cfg['database']['user'],
                                password=cfg['database']['pass'],
                                host=cfg['database']['host']
                                )

        df.to_sql(cfg['database']['table'],
                  con, if_exists='append', index=False)


# %% upload hour and minute localtime offset to the server
def timezone_sync(cfg):
    ts = time.time()
    t = int((datetime.fromtimestamp(ts) -
             datetime.utcfromtimestamp(ts)).total_seconds())

    hours = t // 3600
    minutes = (t % 3600) // 60
    headers = {'tz_hour': str(hours), 'tz_minute': str(minutes)}
    headers['Connection'] = 'close'
    url = f'http://{cfg["server"]["host"]}:{cfg["server"]["port"]}/{cfg["server"]["put_timezone"]}'
    requests.put(url,
                 headers=headers,
                 timeout=60)


# %% main infinite loop
def main():
    global INFINITE_LOOP

    with open(CFG_FILE, 'r') as f:
        cfg = json.load(f)

    counter = 0
    while INFINITE_LOOP:
        if counter >= SLEEP_TIME_LOOP:
            counter = 0
            for x in (timezone_sync, temperature_store):
                try:
                    x(cfg)
                except requests.HTTPError:
                    pass
                except requests.Timeout:
                    pass
                except requests.ConnectionError:
                    pass

        counter += 1
        time.sleep(1)


# %%
if __name__ == '__main__':
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    main()
